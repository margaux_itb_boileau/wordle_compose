import androidx.compose.ui.window.Window
import androidx.compose.ui.window.application
import androidx.compose.material.ExperimentalMaterialApi
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.unit.sp
import androidx.compose.material.*
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.layout.*
import androidx.compose.material.MaterialTheme
import androidx.compose.material.MaterialTheme.typography
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.TextStyle
import kotlin.system.exitProcess

@ExperimentalComposeUiApi
fun main() {
    application {
        Window(onCloseRequest = ::exitApplication) {
            var text by remember { mutableStateOf("") }
            var words by remember { mutableStateOf(listOf<List<String>>()) }
            val secret = "hello"
            val solution = secret.uppercase().chunked(1)
            var color:Color
            var tries = 0

            Column(
                verticalArrangement = Arrangement.Top,
                horizontalAlignment = Alignment.CenterHorizontally,
                modifier = Modifier.fillMaxSize().padding(16.dp)
            ) {
                Row(
                    horizontalArrangement = Arrangement.SpaceBetween,
                    modifier = Modifier.fillMaxWidth()
                ) {
                    TextField(
                        value = text,
                        onValueChange = { text = it },
                        label = { Text("Enter a 5-letter word") },
                        modifier = Modifier.weight(1f)
                    )
                    Spacer(modifier = Modifier.width(16.dp))
                    Button(
                        onClick = {
                            if (text.length == 5) {
                                val chars = text.uppercase().chunked(1)
                                words+=(listOf(chars))
                                text = ""
                            }
                        }
                    ) {
                        Text("Enter")
                    }
                }
                Spacer(modifier = Modifier.height(16.dp))
                for (word in words) {
                    tries ++
                    var count = 0
                    Row(
                        horizontalArrangement = Arrangement.Center,
                        modifier = Modifier.fillMaxWidth()
                    ){
                        for (i in word.indices) {
                            if (word[i] == solution[i]) {
                                color = Color.Green
                            }
                            else if (word[i] in solution) {
                                if (repetitions(word,word[i])<=repetitions(solution,word[i])) {
                                    color = Color.Yellow
                                }
                                else if (count<repetitions(solution,word[i])-timesGuessed(solution,word,word[i])) {
                                    color = Color.Yellow
                                    count++
                                }
                                else color = Color.LightGray
                            }
                            else {
                                color = Color.LightGray
                            }
                            Box(
                                modifier = Modifier
                                    .size(50.dp)
                                    .background(color)
                                    .border(width = 1.dp, color = Color.Black)
                                    .padding(8.dp),
                                contentAlignment = Alignment.Center
                            ) {
                                Text(
                                    text = word[i],
                                    style = TextStyle(fontSize = 20.sp)
                                )
                            }
                        }
                    }
                    Spacer(modifier = Modifier.height(8.dp))
                    if (word==solution) {
                        dialog("Correct", "You won! :D")
                    }
                    if (tries == 6) {
                        dialog("No tries left", "You lost :(")
                    }
                }
            }
        }
    }
}
@OptIn(ExperimentalMaterialApi::class)
@ExperimentalComposeUiApi
@Composable
fun dialog(title:String, text:String) {
    AlertDialog(
        onDismissRequest = {  },
        title = { Text(title) },
        text = { Text(text) },
        confirmButton = {
            Button(
                onClick = { exitProcess(0) },
                colors = ButtonDefaults.buttonColors(
                    backgroundColor = MaterialTheme.colors.primary
                )
            ) {
                Text(
                    text = "OK",
                    style = typography.button
                )
            }
        }
    )
}

fun repetitions(str: List<String>, letter: String):Int {
    var n = 0
    for (j in 0..str.lastIndex) {
        if (letter == str[j]) {
            n++
        }
    }
    return(n)
}

fun timesGuessed(firstWord: List<String>, secondWord: List<String>, letter: String):Int {
    var n = 0
    for (i in firstWord.indices) {
        if (firstWord[i] == secondWord[i] && firstWord[i] == letter && secondWord[i] == letter) {
            n++
        }
    }
    return(n)
}